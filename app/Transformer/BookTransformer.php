<?php

namespace App\Transformer;

use App\Book;
use App\User;
use League\Fractal;

class BookTransformer extends Fractal\TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Book $book)
    {
        return [
            'id' => $book->id,
            'name' => $book->name,
            'isbn' => $book->isbn,
            'created_at' => (string)$book->created_at,
            'updated_at' => (string)$book->updated_at,
            'deleted_at' => (string)$book->deleted_at,
            'created_by' => $book->user_id,
        ];
    }

    /**
     * Include User
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Book $book)
    {
        $user = $book->user;

        return $user instanceof User ? $this->item($user, new UserTransformer) : null;
    }
}
