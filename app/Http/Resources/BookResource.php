<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $ret = [];

        if ( isset($this->id) ) {
            $ret = [
                'id' => $this->id,
                'name' => $this->name,
                'isbn' => $this->isbn,
                'created_at' => (string)$this->created_at,
                'updated_at' => (string)$this->updated_at,
                'deleted_at' => (string)$this->deleted_at,
                'created_by' => $this->user_id
            ];

            if ( $this->user ) {
                $ret['user'] = [
                    'data' => [
                        'id' => $this->user->id,
                        'email' => $this->user->email
                    ]
                ];
            }
        }

        return $ret;
    }
}
