<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Book;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformer\BookTransformer;
// use App\Http\Resources\BookResource;
use App\Rules\BookNameUppercase;
use League\Fractal;

class BookController extends Controller
{
    protected $fractal;

    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);

        $this->fractal = new Fractal\Manager();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginator = Book::paginate(10);
        $books = $paginator->getCollection();
        $resource = new Collection($books, new BookTransformer());
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        // return BookResource::collection(Book::paginate(10));
        return $this->fractal->createData($resource)->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => [ 'required', 'string', 'min:3', 'max:100', new BookNameUppercase ],
            'isbn' => [ 'required', 'isbn' ]
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $validatedData = $request->validate($rules);

        $book = Book::create([
            'user_id' => $request->user()->id,
            'name' => $request->name,
            'isbn' => $request->isbn
        ]);

        $resource = new Fractal\Resource\Item($book, new BookTransformer);

        // return new BookResource($book);
        return $this->fractal->parseIncludes('user')->createData($resource)->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::with('user')->find($id);

        $resource = new Fractal\Resource\Item($book, new BookTransformer);

        // return new BookResource($book);
        return $this->fractal->parseIncludes('user')->createData($resource)->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::find($id);

        // check if currently authenticated user is the owner of the book
        if ($request->user()->id !== $book->user_id) {
            return response()->json(['error' => 'You can only edit your own books.'], 403);
        }

        $book->update( $request->only( ['name', 'isbn'] ) );

        $resource = new Fractal\Resource\Item($book, new BookTransformer);

        // return new BookResource($book);
        return $this->fractal->parseIncludes('user')->createData($resource)->toArray();;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();

        return response()->json(null, 204);
    }
}
