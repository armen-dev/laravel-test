<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach ( range( 1, 500000 ) as $index ) {

            $creadedBy = rand( 0, 1 ) ? DB::table('users')->get()->random()->id : null;

            $randomDate = $faker->dateTimeBetween( '-2 years', 'now' );
            DB::table('books')->insert([
                'name' => strtoupper( rtrim( $faker->sentence( rand( 1, 4 ) ), '.' ) ),
                'isbn' => $faker->isbn13,
                'user_id' => $creadedBy,
                'created_at' => $randomDate,
                'updated_at' => $randomDate
            ]);
        }
    }
}
