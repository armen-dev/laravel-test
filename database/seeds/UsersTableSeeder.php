<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach ( range( 1, 100 ) as $index ) {
            $randomDate = $faker->dateTimeBetween( '-2 years', 'now' );
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'email_verified_at' => $randomDate,
                'password' => bcrypt( 'secret' ),
                'created_at' => $randomDate,
                'updated_at' => $randomDate
            ]);
        }
    }
}
